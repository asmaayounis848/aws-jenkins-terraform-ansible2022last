Jenkins-Ansible Task on AWS
Create ansible script to configure application ec2(private)

configure ansible to run over private ips through bastion (~/.ssh/config)

write ansible script to configure ec2 to run  as jenkins slaves

configure slave in jenkins dashboard (with private ip)

create pipeline to deploy nodejs_example fro branch (rds_redis)

add application load balancer to your terraform code to expose your nodejs app on port 80 on the load balancer

test your application by calling loadbalancer_url/db and /redis


Provision infrastructure on aws
```
terraform init
terraform plan --var-file prod.tfvars
terraform apply --var-file prod.tfvars
```

***
### To access the private instance directly from your local device >> we use Jump host
### Configurations for the jump host:

```bash
$ vim ~/.ssh/config
host bastion
   HostName 3.70.179.172
   User ubuntu
   identityFile ~/.ssh/asmaa.pem

host private_instance
   HostName  10.0.3.106
   user  ubuntu
   ProxyCommand ssh bastion -W %h:%p
   identityFile ~/.ssh/asmaa.pem                                        
```

### Now we need to create infrastructure to run Jenkins-agent on the private EC2
- 1) Using Ansible playbook to install (Docker ,  Open JDK , git)
- 2) Create Home Directory for Jenkins (jenkins_home)
- 3) in the inventory file using the name of the private instance you created on the config file 
### To run the playbook:
```bash
$ ansible-playbook myapp.yaml
```
### Now we create the basic configuration on the Private instance to create jenkins-agent on it
### to let the jenkins master running on local machine can run on the jenkins agent directly we need to make the same configuration for (.ssh/config) file on the jenkins container
```bash
$  docker exec -it (container-id) bash
 root@f5365ab02149:~/ vim .ssh/config
```

```bash
host bastion
   HostName 3.70.179.172
   User ubuntu
   identityFile ~/.ssh/asmaa.pem

host private_instance
   HostName  10.0.3.106
   user  ubuntu
   ProxyCommand ssh bastion -W %h:%p
   identityFile ~/.ssh/asmaa.pem         
```
### Check you can access the private instance through the jenkins container
```bash
root@f5365ab02149:~/.ssh# ssh private_instance
Welcome to Ubuntu 20.04.4 LTS (GNU/Linux 5.13.0-1031-aws x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

  System information as of Sat Jul 30 17:53:42 UTC 2022

  System load:  0.0               Processes:                121
  Usage of /:   57.2% of 7.58GB   Users logged in:          0
  Memory usage: 42%               IPv4 address for docker0: 172.17.0.1
  Swap usage:   0%                IPv4 address for eth0:    10.0.3.106

 * Ubuntu Pro delivers the most comprehensive open source security and
   compliance features.

   https://ubuntu.com/aws/pro

3 updates can be applied immediately.
To see these additional updates run: apt list --upgradable


*** System restart required ***
Last login: Sat Jul 30 16:24:06 2022 from 10.0.1.44
ubuntu@ip-10-0-3-106:~$ 
```
***
### Create the jenkins-node
- ![My image](./img1.png)

- ![My image](./img2.png)

- ![My image](./img3.png)

- ![My image](./img4.png)



